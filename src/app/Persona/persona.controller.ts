import { PersonaModel } from '../../models/personaModel';
export class PersonaController {
    constructor(){
    }

    async crearNombre(persona: PersonaModel) {
        return `Nombre: ${persona.nombre} ${persona.apellido}`;
    }

    async retornarPersona(nombre: string, apellido: string) {
        return new PersonaModel(nombre, apellido);
    }
}