export class PersonaModel {
    constructor(nombre: string, apellido: string) {
        this.nombre = nombre;
        this.apellido = apellido;
    }

    nombre: string;
    apellido: string;
    documento: string;
    tipoDocumento: string;
    genero: string;
    edad: number;
}